.. contents:: **Table of Contents**

summary
=======

This project has no real content: it's just about testing--empirically--how BB repositories support internal/relative links. Some known issues relating to this include:

* `Issue #2323 <https://bitbucket.org/site/master/issues/2323>`_ (open, but in the process of resolution)
* `Issue #6315 <https://bitbucket.org/site/master/issues/6315>`_ (resolved)
* `Issue #12192 <https://bitbucket.org/site/master/issues/12192>`_ (open)

definitions
===========

The following are *terms of art* in the context of this project:

absolute style
--------------

in filesystems, Unix-style `absolute paths <https://en.wikipedia.org/wiki/Path_%28computing%29#Absolute_and_relative_paths>`_ begin with a ``/`` indicating the root directory. The absolute path to a web resource (such as an online repository) is a `URI <https://en.wikipedia.org/wiki/Uniform_resource_identifier>`_. These can be cumbersome (hard to consume, not portable) for web-content authors.

relative style
--------------

Unix-style `relative paths <https://en.wikipedia.org/wiki/Path_%28computing%29#Absolute_and_relative_paths>`_ (for both filesystem and web resources) can begin with

1. ``.`` indicating that the path begins at the current working directory (*CWD*).
#. ``..`` indicating that the path begins at the parent of the CWD.
#. a filename or directory name, which is interpreted as if ``../`` was prepended to the path.

Bitbucket style
---------------

This was an idiosyncrasy once supported by `Bitbucket <https://bitbucket.org/>`_ from project ``README``\s viewed from page=Overview, but ceased to work sometime before last commit.

currently-working style
-----------------------

Whatever seems to be working, at the time of last commit. No guarantees that this will not be subsequently broken by `Bitbucket <https://bitbucket.org/>`_ :-(

project-absolute style
----------------------

for a project-absolute-style path, the leading ``/`` indicates the root directory *of the current project*, thus providing the utility of a traditional absolute path without most of the disadvantages incurred in a web context.

wiki-absolute style
-------------------

Bitbucket (et al?) implement projects and their wikis via separate repositories. Since a wiki has a different repository than its (conceptually) containing project, we can consider the leading ``/`` in a *wiki*\-absolute-style path as indicating the root directory *of the wiki*, not its project.

details
=======

arbitrary file in project repo
------------------------------

We want to be able to link to *ordinary* files in the "main" project repo, i.e., objects which are **not** in its

* downloads
* issues
* wiki

project-absolute style
~~~~~~~~~~~~~~~~~~~~~~

1. a file in the project's top-level folder: `</file_in_project_root.txt>`_
#. a file in a child of the project's top-level folder: `</subdir_level_1/file_in_folder_just_below_project_root.rst>`_

relative style
~~~~~~~~~~~~~~

All `"currently-working" <rst_link_test#rst-header-currently-working-style>`_:

1. from a file in the project's top-level folder:

   1. path starting with a dot: `<./file_in_project_root.txt>`_
   #. path=filename: `<file_in_project_root.txt>`_

#. from a file in a child of the project's top-level folder:

   1. path starting with a dot: `<./subdir_level_1/file_in_folder_just_below_project_root.rst>`_
   #. path starting with first folder name: `<subdir_level_1/file_in_folder_just_below_project_root.rst>`_

#. from an issue: see `<../../issues/2>`_

Bitbucket style
~~~~~~~~~~~~~~~

1. a file in the project's top-level folder: `<rst_link_test/file_in_project_root.txt>`_
#. a file in a child of the project's top-level folder: `<rst_link_test/subdir_level_1/file_in_folder_just_below_project_root.rst>`_

downloads
---------

We've got downloads! How to reference them?

project-absolute style
~~~~~~~~~~~~~~~~~~~~~~

1. page listing current downloads: `</downloads>`_
#. a specific download: `</downloads/downloadable_1.txt>`_

relative style
~~~~~~~~~~~~~~

These should probably fail, since downloads are project resources (not in the filetree):

1. page listing current downloads: `<downloads>`_
#. a specific download: `<downloads/downloadable_1.txt>`_

currently-working style
!!!!!!!!!!!!!!!!!!!!!!!

1. page listing current downloads: `<../../downloads>`_
#. a specific download: `<../../downloads/downloadable_1.txt>`_

Bitbucket style
~~~~~~~~~~~~~~~

1. page listing current downloads: `<rst_link_test/downloads>`_
#. a specific download: `<rst_link_test/downloads/downloadable_1.txt>`_

issues
------

We've got issues! How to reference them?

project-absolute style
~~~~~~~~~~~~~~~~~~~~~~

1. page listing current issues: `</issues>`_
#. page for creating new issue: `</issues/new>`_
#. page for specific issue#=1: `</issue/1>`_

relative style
~~~~~~~~~~~~~~

These should probably fail, since issues are project resources (not in the filetree):

1. page listing current issues: `<issues>`_
#. page for creating new issues: `<issues/new>`_
#. page for issue#=1: `<issue/1>`_

currently-working style
!!!!!!!!!!!!!!!!!!!!!!!

1. page listing current issues: `<../../issues>`_
#. page for creating new issues: `<../../issues/new>`_
#. page for issue#=1: `<../../issue/1>`_

Bitbucket style
~~~~~~~~~~~~~~~

1. page listing current issues: `<rst_link_test/issues>`_
#. page for creating new issues: `<rst_link_test/issues/new>`_
#. page for issue#=1: `<rst_link_test/issue/1>`_

wiki pages
----------

We've got a wiki! How to reference its pages?

project-absolute style
~~~~~~~~~~~~~~~~~~~~~~

1. top-level wikipage:

   1. filename with file extension: `</wiki/Home.rst>`_
   #. without file extension: `</wiki/Home>`_

#. another wikipage in its top-level folder:

   1. filename with file extension: `</wiki/another_wikipage_in_top-level_folder.rst>`_
   #. without file extension: `</wiki/another_wikipage_in_top-level_folder>`_

#. wikipage in a child folder/subdir:

   1. filename with file extension: `</wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
   #. without file extension: `</wiki/subdir_level_2/wikipage_in_child_folder>`_

relative style
~~~~~~~~~~~~~~

1. top-level wikipage:

   1. path starting with a dot:

      1. with file extension: `<./wiki/Home.rst>`_
      #. without file extension: `<./wiki/Home>`_

   #. `currently-working style <rst_link_test#rst-header-currently-working-style>`_:

      1. with file extension: `<../../wiki/Home.rst>`_
      #. without file extension: `<../../wiki/Home>`_

   #. path starting with folder name:

      1. with file extension: `<wiki/Home.rst>`_
      #. without file extension: `<wiki/Home>`_

#. another wikipage in its top-level folder:

   1. path starting with a dot:

      1. with file extension: `<./wiki/another_wikipage_in_top-level_folder.rst>`_
      #. without file extension: `<./wiki/another_wikipage_in_top-level_folder>`_

   #. `currently-working style <rst_link_test#rst-header-currently-working-style>`_:

      1. with file extension: `<../../wiki/another_wikipage_in_top-level_folder.rst>`_
      #. without file extension: `<../../wiki/another_wikipage_in_top-level_folder>`_

   #. path starting with folder name:

      1. with file extension: `<wiki/another_wikipage_in_top-level_folder.rst>`_
      #. without file extension: `<wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

   1. path starting with a dot:

      1. with file extension: `<./wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
      #. without file extension: `<./wiki/subdir_level_2/wikipage_in_child_folder>`_

   #. `currently-working style <rst_link_test#rst-header-currently-working-style>`_:

      1. with file extension: `<../../wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
      #. without file extension: `<../../wiki/subdir_level_2/wikipage_in_child_folder>`_

   #. path starting with folder name:

      1. with file extension: `<wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
      #. without file extension: `<wiki/subdir_level_2/wikipage_in_child_folder>`_

Bitbucket style
~~~~~~~~~~~~~~~

1. top-level wikipage:

   1. with file extension: `<rst_link_test/wiki/Home.rst>`_
   #. filename without file extension: `<rst_link_test/wiki/Home>`_

#. another wikipage in its top-level folder:

   1. filename with file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder.rst>`_
   #. without file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

   1. filename with file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
   #. without file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder>`_

an image
--------

in downloads
~~~~~~~~~~~~

.. |caption| replace:: If you see ``alternate text`` instead of an image, the path attempt failed.

project-absolute style
!!!!!!!!!!!!!!!!!!!!!!

* ``/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

relative style
!!!!!!!!!!!!!!

* ``./downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

* ``downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

currently-working style
@@@@@@@@@@@@@@@@@@@@@@@

* ``../../downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ../../downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

* ``downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ../../downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

Bitbucket style
!!!!!!!!!!!!!!!

* ``rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

in project repo
~~~~~~~~~~~~~~~

project-absolute style
!!!!!!!!!!!!!!!!!!!!!!

`currently-working <rst_link_test#rst-header-currently-working-style>`_:

* ``/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

relative style
!!!!!!!!!!!!!!

`currently-working <rst_link_test#rst-header-currently-working-style>`_:

* ``./images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

* ``images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|

Bitbucket style
!!!!!!!!!!!!!!!

* ``rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   |caption|
